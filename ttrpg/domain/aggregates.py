from datetime import datetime
from enum import IntEnum, StrEnum
from itertools import cycle
from random import randint
from typing import Any, Generator, Optional

from pydantic import BaseModel, Field, validator


class Aggregate(BaseModel):
    ...


class Entity(BaseModel):
    ...


class Protocol(BaseModel):
    ...


class Ability(StrEnum):
    STRENGTH = "strength"
    DEXTERITY = "dexterity"
    CONSTITUTION = "constitution"
    INTELLIGENCE = "intelligence"
    WISDOM = "wisdom"
    CHARISMA = "charisma"


class HitDC(BaseModel):
    def is_success(self, **kwargs) -> bool:
        raise NotImplementedError("HitDC.result() is not implemented")


class Effect(StrEnum):
    ...


class Damage(Effect):
    BLUDGEONING = "bludgeoning"
    PIERCING = "piercing"
    SLASHING = "slashing"
    FIRE = "fire"
    LIGHTNING = "lightning"
    THUNDER = "thunder"
    COLD = "cold"
    POISON = "poison"
    NECROTIC = "necrotic"
    RADIANT = "radiant"
    ACID = "acid"
    PSYCHIC = "psychic"
    FORCE = "force"


class Condition(Effect):
    BLINDED = "blinded"
    CHARMED = "charmed"
    DEAFENED = "deafened"
    FRIGHTENED = "frightened"
    GRAPPLED = "grappled"
    INCAPACITATED = "incapacitated"
    INVISIBLE = "invisible"
    PARALYZED = "paralyzed"
    PETRIFIED = "petrified"
    POISONED = "poisoned"
    PRONE = "prone"
    RESTRAINED = "restrained"
    STUNNED = "stunned"
    UNCONSCIOUS = "unconscious"


class Healing(Effect):
    ALL = "all"


class DamageDealt(Protocol):
    amount: int
    effect: Damage


class CharacterRace(StrEnum):
    HUMAN = "human"
    ELF = "elf"
    DWARF = "dwarf"
    GOBLIN = "goblin"


class CharacterRole(StrEnum):
    FIGHTER = "fighter"
    WIZARD = "wizard"
    ROGUE = "rogue"
    WARRIOR = "warrior"


class Dice(IntEnum):
    D4 = 4
    D6 = 6
    D8 = 8
    D10 = 10
    D12 = 12
    D20 = 20


class DiceCount(HitDC):
    dice: Dice
    count: int

    def roll(self) -> int:
        total = 0
        for _ in range(self.count):
            total += randint(1, self.dice)
        return total


class AttackRoll(HitDC):
    dice: DiceCount = Field(default=DiceCount(dice=Dice.D20, count=1))
    bonus: int = Field(default=0)

    def is_success(self, **kwargs) -> bool:
        return self.dice.roll() + self.bonus >= kwargs["target_defense"]


class SaveRoll(HitDC):
    to_beat: int = Field(default=10)

    def is_success(self, **kwargs) -> bool:
        return self.to_beat >= kwargs["save_result"]


class NoRoll(HitDC):
    def is_success(self, **kwargs) -> bool:
        return True


class ValueObject(BaseModel):
    ...


class Action(ValueObject):
    effect: Effect
    effect_dice: list[DiceCount] = Field(default_factory=list)
    attack_roll: AttackRoll = Field(default_factory=AttackRoll)

    def hits(self, target_defense: int = 0) -> bool:
        return self.attack_roll.is_success(target_defense=target_defense)

    def roll_effect(self) -> int:
        total = 0
        for dice_count in self.effect_dice:
            for _ in range(dice_count.count):
                total += randint(1, dice_count.dice)
        return total


class Attack(Action):
    effect: Damage
    effect_dice: list[DiceCount]

    def do_damage(self, target: "Character") -> DamageDealt:
        damage_amount = self.roll_effect()
        damage = target.take_damage(self.effect, damage_amount)
        return DamageDealt(amount=damage, effect=self.effect)


class GreatAxe(Attack):
    effect: Damage = Damage.SLASHING
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D12, count=1)]


class Dagger(Attack):
    effect: Damage = Damage.PIERCING
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D4, count=1)]


class UnarmedStrike(Attack):
    effect: Damage = Damage.BLUDGEONING
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D12, count=1)]


class Spell(ValueObject):
    effect: Effect
    hit_dc: HitDC = Field(default_factory=NoRoll)
    effect_dice: list[DiceCount] = Field(default_factory=list)
    roll_bonus: int = Field(default=0)

    def roll_effect(self) -> int:
        total = 0
        for dice_count in self.effect_dice:
            total += dice_count.roll()
        return total + self.roll_bonus

    def cast_effect(self, target: "Character") -> None:
        if isinstance(self.effect, Damage):
            target.take_damage(self.effect, self.roll_effect())
        elif isinstance(self.effect, Healing):
            target.gain_hits(self.roll_effect())
        else:
            raise NotImplementedError(
                f"Spell.cast_effect() is not implemented for the effect type {self.effect}"
            )


class HealingSpell(Spell):
    effect: Healing = Healing.ALL
    hit_dc: HitDC = NoRoll()


class HealingWord(HealingSpell):
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D4, count=1)]


class Fireball(Spell):
    effect: Damage = Damage.FIRE
    hit_dc: HitDC = AttackRoll()
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D6, count=2)]


class SacredFlame(Spell):
    effect: Damage = Damage.RADIANT
    hit_dc: HitDC = SaveRoll()
    effect_dice: list[DiceCount] = [DiceCount(dice=Dice.D8, count=1)]


class Save(BaseModel):
    ability: Ability
    value: int


class SpellBook(BaseModel):
    spells: list[Spell] = Field(default_factory=list)

    def add_spell(self, spell: Spell) -> None:
        self.spells.append(spell)

    def remove_spell(self, spell: Spell) -> None:
        self.spells.remove(spell)

    def contains_spell(self, spell: Spell) -> bool:
        return spell in self.spells


class Character(Entity):
    name: str
    race: CharacterRace
    role: CharacterRole
    defense: int = Field(default=10)
    hits: int = Field(default=10)
    resistances: list[Damage] = Field(default_factory=list)
    immunities: list[Damage] = Field(default_factory=list)
    actions: list[Action] = Field(default_factory=list)
    spells: SpellBook = Field(default_factory=SpellBook)

    @property
    def is_alive(self) -> bool:
        return self.hits > 0

    @property
    def is_unconscious(self) -> bool:
        return not self.is_alive

    def is_immune_to(self, damage_type: Damage) -> bool:
        return damage_type in self.immunities

    def is_resistant_to(self, damage_type: Damage) -> bool:
        return damage_type in self.resistances

    def knows_spell(self, spell: Spell) -> bool:
        return self.spells.contains_spell(spell)

    def attack(self, target: "Character", attack: Attack) -> DamageDealt:
        damage = 0
        if attack.hits(target.defense):
            return attack.do_damage(target)
        return DamageDealt(amount=damage, effect=attack.effect)

    def gain_hits(self, amount: int) -> None:
        self.hits += amount

    def take_damage(self, effect: Damage, amount: int) -> int:
        if self.is_immune_to(effect):
            total_damage = 0
        elif self.is_resistant_to(effect):
            total_damage = amount // 2
        else:
            total_damage = amount

        if total_damage > self.hits:
            self.hits = 0
        else:
            self.hits -= total_damage
        return total_damage

    def inflict_damage(
        self, character: "Character", effect: Damage, amount: int
    ) -> int:
        return character.take_damage(effect, amount)

    def learn_spell(self, spell: Spell) -> None:
        self.spells.add_spell(spell)

    def unlearn_spell(self, spell: Spell) -> None:
        self.spells.remove_spell(spell)

    def add_action(self, action: Action) -> None:
        self.actions.append(action)

    def remove_action(self, action: Action) -> None:
        self.actions.remove(action)

    # TODO: return CastResult, like DamageDealt
    def cast(self, spell: Spell, target: "Character", save_result: int = 0) -> bool:
        if spell.hit_dc.is_success(
            target_defense=target.defense, save_result=save_result
        ):
            spell.cast_effect(target)
            return True
        return False


class Player(Entity):
    name: str
    characters: list[Character] = Field(default_factory=list)

    def add_character(self, character: Character) -> None:
        self.characters.append(character)

    def remove_character(self, character: Character) -> None:
        self.characters.remove(character)

    def select_character(self, character_name: str) -> Character:
        for character in self.characters:
            if character.name == character_name:
                return character
        raise NotImplementedError(f"Character {character_name} not found")


class Turn(BaseModel):
    character: Character
    initiative: int


class Initiative(BaseModel):
    characters: list[Character]
    order: list[Turn] = Field(default_factory=list)
    cycle: Generator[Turn, None, None] = Field(default=iter([]))

    @validator("order", always=True)
    def _calculate_order_from_characters(cls, _, values: dict[str, Any]) -> list[Turn]:
        assert isinstance(values["characters"], list)
        d20 = DiceCount(dice=Dice.D20, count=1)
        character_turns = [
            Turn(character=character, initiative=d20.roll())
            for character in values["characters"]
        ]
        return sorted(character_turns, key=lambda ct: ct.initiative, reverse=True)

    @validator("cycle", always=True)
    def _init_calculate_order_generator(
        cls,
        _,
        values: dict[str, Any],
    ) -> Generator[list[tuple[Character, int]], None, None]:
        return (order for order in cycle(values["order"]))


class Encounter(Entity):
    name: str
    characters: list[Character] = Field(default_factory=list)
    player_characters: list[Character] = Field(default_factory=list)
    non_player_characters: list[Character] = Field(default_factory=list)
    start_time: Optional[datetime] = None
    end_time: Optional[datetime] = None
    initiative: Optional[Initiative] = None
    current_round: Optional[int] = None
    current_turn: Optional[Turn] = None

    @classmethod
    def begin(
        cls,
        *,
        name: str,
        player_characters: list[Character],
        non_player_characters: list[Character],
    ) -> "Encounter":
        characters = player_characters + non_player_characters
        return cls(
            name=name,
            characters=characters,
            start_time=datetime.now(),
            player_characters=player_characters,
            non_player_characters=non_player_characters,
        )

    @property
    def is_active(self) -> bool:
        return self.start_time is not None and self.end_time is None

    def end(self) -> None:
        self.end_time = datetime.now()

    def add_character(self, character: Character) -> None:
        self.characters.append(character)

    def remove_character(self, character: Character) -> None:
        self.characters.remove(character)

    def determine_initiative(self) -> None:
        self.initiative = Initiative(characters=self.characters)

    def start_combat(self) -> None:
        self.determine_initiative()
        self.current_round = 1
        assert self.initiative
        self.current_turn = next(self.initiative.cycle)

    def end_combat(self) -> None:
        self.current_round = None
        self.current_turn = None
        self.initiative = None

    def end_turn(self) -> None:
        assert self.initiative
        self.current_turn = next(self.initiative.cycle)


class Session(Entity):
    name: str
    players: list[Player] = Field(default_factory=list)
    encounters: list[Encounter] = Field(default_factory=list)
    start_time: datetime = Field(default_factory=datetime.now)
    end_time: Optional[datetime] = None

    class ActiveEncounterExistsError(Exception):
        ...

    @property
    def active_encounter(self) -> Optional[Encounter]:
        for encounter in self.encounters:
            if encounter.is_active:
                return encounter
        return None

    def end(self) -> None:
        for encounter in self.encounters:
            encounter.end()
        self.end_time = datetime.now()

    def start_encounter(
        self,
        name: str,
        player_characters: list[Character],
        non_player_characters: list[Character],
    ) -> Encounter:
        if self.active_encounter is not None:
            raise self.ActiveEncounterExistsError(
                "An active encounter already exists for this session"
            )
        encounter = Encounter.begin(
            name=name,
            player_characters=player_characters,
            non_player_characters=non_player_characters,
        )
        self.encounters.append(encounter)
        return encounter

    def end_encounter(self) -> None:
        if self.active_encounter is not None:
            self.active_encounter.end()

    def add_player(self, player: Player) -> None:
        self.players.append(player)

    def remove_player(self, player: Player) -> None:
        self.players.remove(player)


class Game(Aggregate):
    name: str
    sessions: list[Session] = Field(default_factory=list)
    players: list[Player] = Field(default_factory=list)

    def start_session(self, name: str) -> Session:
        session = Session(name=name, players=self.players)
        self.sessions.append(session)
        return session
