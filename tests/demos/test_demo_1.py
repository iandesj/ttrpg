import random
from ttrpg.domain.aggregates import (
    Character,
    CharacterRace,
    CharacterRole,
    Dagger,
    Damage,
    Fireball,
    Game,
    GreatAxe,
    Player,
    SpellBook,
)


def test_demo_1():
    player_one = Player(
        name="Johnny",
        characters=[
            Character(
                name="Zex",
                race=CharacterRace.ELF,
                role=CharacterRole.WIZARD,
                resistances=[Damage.ACID],
                actions=[Dagger()],
                spells=SpellBook(spells=[Fireball()]),
            ),
        ],
    )
    player_two = Player(
        name="Kara",
        characters=[
            Character(
                name="Violenda",
                race=CharacterRace.HUMAN,
                role=CharacterRole.ROGUE,
                resistances=[],
                actions=[Dagger()],
                spells=SpellBook(spells=[Fireball()]),
            ),
        ],
    )
    game = Game(name="Demo Game One", players=[player_one, player_two])
    session = game.start_session(name="Demo Session One")
    session.add_player(player_one)
    session.add_player(player_two)
    goblin = Character(
        name="Goblin",
        race=CharacterRace.GOBLIN,
        role=CharacterRole.WARRIOR,
        resistances=[Damage.ACID],
        actions=[GreatAxe()],
    )
    zex = player_one.select_character("Zex")
    violenda = player_two.select_character("Violenda")
    encounter = session.start_encounter(
        name="Demo Encounter One",
        player_characters=[zex, violenda],
        non_player_characters=[goblin],
    )
    encounter.start_combat()

    while goblin.hits > 0:
        if encounter.current_turn.character is zex:
            result = zex.cast(Fireball(), target=goblin)
            print(result)

        if encounter.current_turn.character is violenda:
            result = violenda.attack(target=goblin, attack=Dagger())
            print(result)

        if encounter.current_turn.character is violenda:
            result = violenda.attack(
                target=random.choice(encounter.player_characters), attack=Dagger()
            )
            print(result)
        encounter.end_turn()
    encounter.end_combat()
    print(zex.hits)
    print(violenda.hits)
    print(goblin.hits)
    encounter.end()
    session.end
