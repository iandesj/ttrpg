import pytest

from ttrpg.domain.aggregates import (
    Character,
    CharacterRace,
    CharacterRole,
    Damage,
    DamageDealt,
    Effect,
    Encounter,
    Fireball,
    Game,
    GreatAxe,
    HealingWord,
    Player,
    SacredFlame,
    Session,
    SpellBook,
    UnarmedStrike,
)


@pytest.fixture
def game() -> Game:
    return Game(name="Game 1")


@pytest.fixture
def session(game: Game) -> Session:
    return game.start_session(name="Session 1")


@pytest.fixture
def encounter(session: Session) -> Encounter:
    return session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )


@pytest.fixture
def character() -> Character:
    return Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        hits=10,
        role=CharacterRole.FIGHTER,
        defense=12,
        actions=[],
        spells=SpellBook(),
    )


@pytest.fixture
def player(character: Character) -> Player:
    return Player(name="Player 1", characters=[character])


def test_game__start_session(player: Player) -> None:
    game = Game(name="Game 1", players=[player])

    session = game.start_session(name="Session 1")

    assert game.sessions[0] == session


def test_session__start_encounter(session: Session) -> None:
    encounter = session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )

    assert session.encounters[0] == encounter
    assert encounter.start_time is not None


def test_session__start_encounter__raises_when_active_encounter_exists(
    session: Session,
) -> None:
    _ = session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )

    with pytest.raises(Session.ActiveEncounterExistsError):
        session.start_encounter(
            name="Encounter 2", player_characters=[], non_player_characters=[]
        )


def test_session__end_session(session: Session) -> None:
    session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )
    session.end()

    assert session.end_time is not None


def test_session__end_encounter(session: Session) -> None:
    encounter = session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )

    session.end_encounter()

    assert encounter.end_time is not None


def test_session__add_player(session: Session) -> None:
    player = Player(name="Player 1")

    session.add_player(player=player)

    assert session.players[0] == player


def test_session__remove_player(session: Session) -> None:
    player = Player(name="Player 1")
    session.add_player(player=player)

    session.remove_player(player=player)

    assert player not in session.players


def test_encounter__begin() -> None:
    encounter = Encounter.begin(
        name="Encounter 1",
        player_characters=[],
        non_player_characters=[],
    )

    assert encounter.start_time is not None


def test_encounter__add_character(encounter: Encounter, character: Character) -> None:
    encounter.add_character(character=character)

    assert encounter.characters[0] == character


def test_encounter__remove_character(
    encounter: Encounter, character: Character
) -> None:
    encounter.add_character(character=character)

    encounter.remove_character(character=character)

    assert character not in encounter.characters


def test_encounter__end(game: Game) -> None:
    session = game.start_session(name="Session 1")
    encounter = session.start_encounter(
        name="Encounter 1", player_characters=[], non_player_characters=[]
    )

    encounter.end()

    assert encounter.end_time is not None


def test_encounter__determine_initiative(encounter: Encounter) -> None:
    character1 = Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character1)
    character2 = Character(
        name="Character 2",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character2)

    encounter.determine_initiative()

    assert encounter.initiative


def test_encounter__start_combat(encounter: Encounter) -> None:
    character1 = Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character1)
    character2 = Character(
        name="Character 2",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character2)
    encounter.determine_initiative()
    assert encounter.initiative

    encounter.start_combat()

    assert encounter.current_round == 1


def test_encounter__end_turn(encounter: Encounter) -> None:
    character1 = Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character1)
    character2 = Character(
        name="Character 2",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character2)
    encounter.determine_initiative()
    assert encounter.initiative
    encounter.start_combat()
    assert encounter.current_turn == encounter.initiative.order[0]

    encounter.end_turn()

    assert encounter.current_turn == encounter.initiative.order[1]


def test_encounter__end_combat(encounter: Encounter) -> None:
    character1 = Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character1)
    character2 = Character(
        name="Character 2",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
    )
    encounter.add_character(character=character2)
    encounter.determine_initiative()
    assert encounter.initiative
    encounter.start_combat()

    encounter.end_combat()

    assert encounter.current_round is None
    assert encounter.current_turn is None
    assert encounter.initiative is None


def test_player__add_character(character: Character) -> None:
    player = Player(name="Player 1")

    player.add_character(character=character)

    assert player.characters[0] == character


def test_player__select_character__exists(character: Character) -> None:
    player = Player(name="Player 1")
    player.add_character(character=character)

    selected_character = player.select_character(character_name=character.name)

    assert selected_character == character


def test_player__select_character__doesnt_exist(character: Character) -> None:
    player = Player(name="Player 1")

    with pytest.raises(NotImplementedError):
        player.select_character(character_name=character.name)


def test_player__remove_character(character: Character) -> None:
    player = Player(name="Player 1")
    player.add_character(character=character)

    player.remove_character(character=character)

    assert character not in player.characters


def test_character__take_damage(character: Character) -> None:
    damage = DamageDealt(amount=5, effect=Damage.BLUDGEONING)

    damage_taken = character.take_damage(damage.effect, damage.amount)

    assert damage_taken == 5
    assert character.hits == 5


def test_character__take_damage__negative(character: Character) -> None:
    damage = DamageDealt(amount=12, effect=Damage.BLUDGEONING)

    damage_taken = character.take_damage(damage.effect, damage.amount)

    assert damage_taken == 12
    assert character.hits == 0


def test_character__take_damage__with_resistance(character: Character) -> None:
    character.resistances = [Damage.BLUDGEONING]
    damage = DamageDealt(amount=5, effect=Damage.BLUDGEONING)

    damage_taken = character.take_damage(damage.effect, damage.amount)

    assert damage_taken == 2
    assert character.hits == 8


def test_character__take_damage__with_immunity(character: Character) -> None:
    character.immunities = [Damage.BLUDGEONING]
    damage = DamageDealt(amount=5, effect=Damage.BLUDGEONING)

    damage_taken = character.take_damage(damage.effect, damage.amount)

    assert damage_taken == 0
    assert character.hits == 10


def test_character__inflict_damage(character: Character) -> None:
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=10,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )
    damage = DamageDealt(amount=5, effect=Damage.BLUDGEONING)

    damage_inflicted = character.inflict_damage(
        target_character, damage.effect, damage.amount
    )

    assert damage_inflicted == 5
    assert target_character.hits == 5


def test_character__is_alive() -> None:
    character = Character(
        name="Character 1",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=10,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    assert character.is_alive


def test_character__is_uncounscious() -> None:
    character = Character(
        name="Character 1",
        hits=0,
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=10,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    assert character.is_unconscious


def test_character__learn_spell(character: Character) -> None:
    spell = Fireball()

    character.learn_spell(spell=spell)

    assert character.knows_spell(spell)


def test_character__unlearn_spell(character: Character) -> None:
    spell = Fireball()
    character.learn_spell(spell=spell)

    character.unlearn_spell(spell=spell)

    assert not character.knows_spell(spell)


def test_character__add_action(character: Character) -> None:
    action = GreatAxe()

    character.add_action(action=action)

    assert character.actions[0] == action


def test_character__remove_action(character: Character) -> None:
    action = GreatAxe()
    character.add_action(action=action)

    character.remove_action(action=action)

    assert action not in character.actions


def test_character__attack__hits(character: Character) -> None:
    attack_action = GreatAxe()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=0,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    attack_result = character.attack(target=target_character, attack=attack_action)

    assert attack_result.effect == attack_action.effect
    assert attack_result.amount > 0


def test_character__attack__misses(character: Character) -> None:
    attack_action = GreatAxe()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=21,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    attack_result = character.attack(target=target_character, attack=attack_action)

    assert attack_result.effect == attack_action.effect
    assert attack_result.amount == 0


def test_character__cast__damage__attack_roll(character: Character) -> None:
    spell_action = Fireball()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=0,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    spell_result = character.cast(target=target_character, spell=spell_action)

    assert spell_result is True


def test_character__cast__damage__save_roll__fails(
    character: Character,
) -> None:
    spell_action = SacredFlame()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=0,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    spell_result = character.cast(
        target=target_character, spell=spell_action, save_result=11
    )

    assert spell_result is False


def test_character__cast__NotImplementedError(character: Character) -> None:
    class NotAnEffect(Effect):
        NOPE = "nope"

    spell_action = HealingWord()
    spell_action.effect = NotAnEffect.NOPE  # type: ignore
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=0,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    with pytest.raises(NotImplementedError):
        character.cast(target=target_character, spell=spell_action)


def test_character__cast__healing(character: Character) -> None:
    spell_action = HealingWord()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=0,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    spell_result = character.cast(target=target_character, spell=spell_action)

    assert spell_result is True
    assert target_character.hits > 10


def test_character__cast__misses(character: Character) -> None:
    spell_action = Fireball()
    target_character = Character(
        name="Character bad",
        race=CharacterRace.HUMAN,
        role=CharacterRole.FIGHTER,
        defense=21,
        actions=[GreatAxe(), UnarmedStrike()],
        spells=SpellBook(spells=[Fireball()]),
    )

    spell_result = character.cast(target=target_character, spell=spell_action)

    assert spell_result is False
